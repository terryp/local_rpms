from rpm import spec, tagnames, specPkg


class SpecPkg:
    def __init__(self, pkg: specPkg):
        self.__pkg = pkg

    def __rich_repr__(self):
        for key in self.__pkg.header.keys():
            yield tagnames[key], self.__pkg.header[key]


class Spec(spec):
    def __rich_repr__(self):
        for key in self.sourceHeader.keys():
            yield tagnames[key], self.sourceHeader[key]

        yield 'packages', [
            SpecPkg(pkg)
            for pkg in self.packages
        ]
