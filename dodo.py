from __future__ import annotations
from os import environ, fspath, PathLike
from pathlib import Path
from pydantic import BaseModel, HttpUrl
from urllib.parse import urlparse
from os.path import basename
from rpm import (
    spec as Spec,
    specPkg as SpecPkg,
    RPMTAG_NEVR,
    RPMTAG_NEVRA,
    RPMTAG_NAME,
    RPMTAG_ARCH,
)

import requests

SRC = Path(environ['SRC'])
spec_files = tuple(SRC.joinpath('specs').glob('*.spec'))


build_root = SRC.joinpath('rpmbuild')

SPECS = build_root.joinpath('SPECS')
SOURCES = build_root.joinpath('SOURCES')
SRPMS = build_root.joinpath('SRPMS')
RPMS = build_root.joinpath('RPMS')

rpmbuild_dirs = (
    'BUILD',
    'RPMS',
    'SOURCES',
    'SPECS',
    'SRPMS',
)


top_dir = f'--define "_topdir {build_root}"'


class PackageSpec(BaseModel):
    name: str
    nerv: str

    @classmethod
    def from_rpm(cls, rpm_spec: SpecPkg) -> PackageSpec:
        return cls(
            name=rpm_spec.header[RPMTAG_NAME],
            nerv=rpm_spec.header[RPMTAG_NEVR],
        )


class SourceSpec(BaseModel):
    name: str
    nerv: str
    nerva: str
    arch: str
    path: Path
    sources: list[tuple[HttpUrl, Path]]
    packages: list[PackageSpec]

    @classmethod
    def from_rpm(cls, rpm_spec: Spec, path: Path) -> SourceSpec:
        sources = [
            (url, cls.source_dep(url))
            for (url, _, _) in rpm_spec.sources
        ]

        return cls(
            name=rpm_spec.sourceHeader[RPMTAG_NAME],
            path=SPECS.joinpath(path.name),
            nerv=rpm_spec.sourceHeader[RPMTAG_NEVR],
            nerva=rpm_spec.sourceHeader[RPMTAG_NEVRA],
            arch=rpm_spec.sourceHeader[RPMTAG_ARCH],
            sources=sources,
            packages=[
                PackageSpec.from_rpm(pkg)
                for pkg in rpm_spec.packages
            ],
        )

    @classmethod
    def from_path(cls, path: PathLike) -> SourceSpec:
        rpm_spec = Spec(fspath(path))

        return cls.from_rpm(rpm_spec, path)

    @staticmethod
    def source_dep(url):
        url_parse = urlparse(url)
        file_name = basename(url_parse.path)

        return SOURCES.joinpath(file_name)

    @property
    def srpm_path(self):
        return SRPMS.joinpath(f'{self.nerv}.src.rpm')

    @property
    def rpm_path(self):
        return RPMS.joinpath(self.arch, f'{self.nerva}.rpm')

    @property
    def source_paths(self):
        return [
            path
            for _, path in self.sources
        ]


specs = tuple(SourceSpec.from_path(path) for path in spec_files)


def create_rpmbuild():
    for dir in rpmbuild_dirs:
        build_root.joinpath(dir).mkdir(parents=True, exist_ok=True)


def copy_spec(spec: Path):
    name = spec.name
    target_path = SPECS.joinpath(name)
    try:
        target_path.hardlink_to(spec)
    except FileExistsError:
        pass


def fetch_source(spec_url, out_file):
    with requests.get(spec_url) as req:
        req.raise_for_status()
        with open(out_file, 'wb') as file:
            for chunk in req.iter_content(chunk_size=8192):
                file.write(chunk)


def task_rpm_directories():
    return dict(
        actions=[create_rpmbuild],
        targets=[
            build_root.joinpath(dir)
            for dir in rpmbuild_dirs
        ]
    )


def task_copy_spec():
    for spec in spec_files:
        yield dict(
            name=spec.name,
            actions=[(copy_spec, (spec,))],
            targets=[SPECS.joinpath(spec.name)],
            file_dep=[spec],
        )


def task_fetch():
    for spec in specs:
        for url, out_file in spec.sources:
            yield dict(
                name=f'{spec.name}:{basename(out_file)}',
                actions=[(fetch_source, (url, out_file))],
                file_dep=[spec.path],
                targets=[out_file],
            )


def task_srpm():
    for spec in specs:
        yield dict(
            name=spec.name,
            actions=[f'rpmbuild {top_dir} -br {spec.path}'],
            targets=[spec.srpm_path],
            file_dep=spec.source_paths + [spec.path],
        )


def task_rpm():
    for spec in specs:
        yield dict(
            name=spec.name,
            file_dep=[spec.srpm_path],
            actions=[f'rpmbuild {top_dir} --rebuild {spec.srpm_path}'],
            targets=[spec.rpm_path],
        )
