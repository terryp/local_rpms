Name: nerd-fonts
Version: 3.2.1
Release: 0%{dist}
URL: https://www.nerdfonts.com/
BuildArch:  noarch

License: MIT
Summary: Various icon fonts compiled into one font.
Source0: https://github.com/ryanoasis/nerd-fonts/releases/download/v3.2.1/NerdFontsSymbolsOnly.zip

%description
Nerd Fonts patches developer targeted fonts with a high number of glyphs
(icons).
Specifically to add a high number of extra glyphs from popular ‘iconic fonts’
such as Font Awesome, Devicons, Octicons, and others.


%prep
%setup -c nerd-fonts-2.3.3

%install
install -D \
    'SymbolsNerdFont-Regular.ttf' \
    'SymbolsNerdFontMono-Regular.ttf' \
    --target-directory=%{buildroot}%{_datadir}/fonts/nerd-fonts

%files
%{_datadir}/fonts/nerd-fonts/SymbolsNerdFont-Regular.ttf
%{_datadir}/fonts/nerd-fonts/SymbolsNerdFontMono-Regular.ttf
%license LICENSE

%changelog
* Thu Feb 27 2025 Terry Patterson <terryp@wegrok.net> - 3.2.1-0
- Upgrade nerd fonts and make files explicit

* Tue Feb 07 2023 Terry Patterson <terryp@wegrok.net> - 2.3.3-1
- Include non-mono font files

* Sat Jan 28 2023 Terry Patterson <terryp@wegrok.net> - 2.3.3-0
- Inital rpm build
